<?xml version="1.0"?>

<recipe>

    <merge from="AndroidManifest.xml.ftl" to="${escapeXmlAttribute(manifestOut)}/AndroidManifest.xml" />

    <#include "activity_layout_recipe.xml.ftl" />

    <instantiate from="src/app_package/classes/Activity.kt.ftl"
    to="${escapeXmlAttribute(srcOut)}/view/${className}Activity.kt" />

    <instantiate from="src/app_package/classes/Interactor.kt.ftl"
    to="${escapeXmlAttribute(srcOut)}/interactor/${className}Interactor.kt" />

    <instantiate from="src/app_package/classes/InteractorImp.kt.ftl"
    to="${escapeXmlAttribute(srcOut)}/interactor/${className}InteractorImp.kt" />

    <instantiate from="src/app_package/classes/ActivityModule.kt.ftl"
        to="${escapeXmlAttribute(srcOut)}/${className}ActivityModule.kt" />

    <instantiate from="src/app_package/classes/Presenter.kt.ftl"
        to="${escapeXmlAttribute(srcOut)}/presenter/${className}Presenter.kt" />

    <instantiate from="src/app_package/classes/PresenterImp.kt.ftl"
        to="${escapeXmlAttribute(srcOut)}/presenter/${className}PresenterImp.kt" />

    <instantiate from="src/app_package/classes/View.kt.ftl"
        to="${escapeXmlAttribute(srcOut)}/view/${className}View.kt" />


</recipe>
