package ${packageName}.interactor

import ${escapeXmlAttribute(package)}.ui.base.interactor.BaseInteractor

interface ${className}Interactor : BaseInteractor {

}