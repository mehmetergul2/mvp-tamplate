package ${packageName}.view

import ${escapeXmlAttribute(package)}.ui.base.view.BaseView

interface ${className}View : BaseView {

    fun fail()
    fun terminate()

}