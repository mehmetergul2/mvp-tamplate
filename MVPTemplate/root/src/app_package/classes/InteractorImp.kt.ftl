package ${packageName}.interactor

import ${escapeXmlAttribute(package)}.data.preferences.PreferenceHelper
import ${escapeXmlAttribute(package)}.network.api.ApiHelperImp
import ${escapeXmlAttribute(package)}.ui.base.interactor.BaseInteractorImp
import javax.inject.Inject

class ${className}InteractorImp @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelperImp: ApiHelperImp) :
    BaseInteractorImp(preferenceHelper = preferenceHelper, apiHelperImp = apiHelperImp), ${className}Interactor {

}