package ${packageName}.presenter

import ${escapeXmlAttribute(package)}.ui.base.presenter.BasePresenter
import ${packageName}.interactor.${className}Interactor
import ${packageName}.view.${className}View

interface ${className}Presenter<V : ${className}View, I : ${className}Interactor> : BasePresenter<V, I> {



}