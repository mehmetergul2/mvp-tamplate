package ${packageName}.presenter

import ${escapeXmlAttribute(package)}.ui.base.presenter.BasePresenterImp
import ${packageName}.interactor.${className}Interactor
import ${packageName}.view.${className}View
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ${className}PresenterImp<V : ${className}View, I : ${className}Interactor> @Inject internal constructor(
    interactor: I,
    disposable: CompositeDisposable
) : BasePresenterImp<V, I>(
    interactor = interactor,
    compositeDisposable = disposable
), ${className}Presenter<V, I> {



    }