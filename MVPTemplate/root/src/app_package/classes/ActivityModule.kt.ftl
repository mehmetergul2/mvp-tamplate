package ${packageName}

import dagger.Module
import dagger.Provides
import ${packageName}.interactor.${className}Interactor
import ${packageName}.interactor.${className}InteractorImp
import ${packageName}.presenter.${className}Presenter
import ${packageName}.presenter.${className}PresenterImp
import ${packageName}.view.${className}View

@Module
class ${className}ActivityModule {

    @Provides
    internal fun provide${className}Interactor(m${className}InteractorImp: ${className}InteractorImp): ${className}Interactor = m${className}InteractorImp

    @Provides
    internal fun provideMainPresenter(m${className}Presenter: ${className}PresenterImp<${className}View, ${className}Interactor>)
    : ${className}Presenter<${className}View, ${className}Interactor> = m${className}Presenter
}
