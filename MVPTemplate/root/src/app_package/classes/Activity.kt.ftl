package ${packageName}.view

import android.os.Bundle
import android.view.ViewGroup
import com.loodos.starter.R
import ${escapeXmlAttribute(package)}.ui.base.view.BaseBottomUpActivity
import ${packageName}.interactor.${className}Interactor
import ${packageName}.presenter.${className}PresenterImp
import kotlinx.android.synthetic.main.${activityLayoutName}.*
import javax.inject.Inject

class ${className}Activity : BaseBottomUpActivity(), ${className}View {

    @Inject
    lateinit var m${className}Presenter: ${className}PresenterImp<${className}View, ${className}Interactor>

    override fun onCreateActivity(savedInstanceState: Bundle?): ViewGroup {
        setContentView(R.layout.${activityLayoutName})
        m${className}Presenter.onAttach(this)
        return root${className}Activity as ViewGroup
    }

    override fun bindView() {
    }

    override fun fail() {
    }

    override fun terminate() {
    }
}
